function varargout = Main(varargin)
% MAIN MATLAB code for Main.fig
%      MAIN, by itself, creates a new MAIN or raises the existing
%      singleton*.
%
%      H = MAIN returns the handle to a new MAIN or the handle to
%      the existing singleton*.
%
%      MAIN('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MAIN.M with the given input arguments.
%
%      MAIN('Property','Value',...) creates a new MAIN or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Main_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Main_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Main

% Last Modified by GUIDE v2.5 16-Jul-2018 21:15:08

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Main_OpeningFcn, ...
                   'gui_OutputFcn',  @Main_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Main is made visible.
function Main_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Main (see VARARGIN)

% Choose default command line output for Main
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Main wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Main_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in rotate.
function rotate_Callback(hObject, eventdata, handles)
i=imread('rayan.jpg');
J = imrotate(i,35,'bilinear');
figure, imshow(J);
% hObject    handle to rotate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in grayScale.
function grayScale_Callback(hObject, eventdata, handles)
i=imread('rayan.jpg');
I=rgb2gray(i);
figure,imshow(I)
% hObject    handle to grayScale (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in binary.
function binary_Callback(hObject, eventdata, handles)
i=imread('rayan.jpg');
I=rgb2gray(i);
BW=im2bw(I);
figure,imshow(BW)
% hObject    handle to binary (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in background.
function background_Callback(hObject, eventdata, handles)
i=imread('rayan.jpg');
I=rgb2gray(i);
BW=im2bw(I);

[n1 n2]=size(BW);
r=floor(n1/10);
c=floor(n2/10);
x1=1;x2=r;
s=r*c;

for i=1:10
    y1=1;y2=c;
    
    for j=1:10
        if (y2<=c || y2>=9*c) || (x1==1 || x2==r*10)
            loc=find(BW(x1:x2, y1:y2)==0);
            [o p]=size(loc);
            pr=o*100/s;

            if pr<=100
                BW(x1:x2, y1:y2)=0;
                r1=x1;r2=x2;s1=y1;s2=y2;
                pr1=0;
            end

            

        end

            y1=y1+c;
            y2=y2+c;

    end

 x1=x1+r;
 x2=x2+r;

end
 figure,imshow(BW)
% hObject    handle to background (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in edge.
function edge_Callback(hObject, eventdata, handles)
i=imread('rayan.jpg');
I=rgb2gray(i);

BW1 = edge(I,'prewitt');
BW2 = edge(I,'canny');
figure, imshow(BW2)
% hObject    handle to edge (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in original.
function original_Callback(hObject, eventdata, handles)
i=imread('rayan.jpg');  % for eg. C:\P1140119.jpg
figure,imshow(i);
% hObject    handle to original (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pixel.
function pixel_Callback(hObject, eventdata, handles)
h = figure, imshow('rayan.jpg');
hp = impixelinfo;
set(hp,'Position',[150 290 300 20]);
% hObject    handle to pixel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in face.
function face_Callback(hObject, eventdata, handles)
i=imread('v.jpeg');
I=rgb2gray(i);
BW=im2bw(I);

L = bwlabel(BW,8);
BB  = regionprops(L, 'BoundingBox');
BB1=struct2cell(BB);
BB2=cell2mat(BB1);

[s1 s2]=size(BB2);
mx=0;

for k=3:4:s2-1

    p=BB2(1,k)*BB2(1,k+1);

    if p>mx && (BB2(1,k)/BB2(1,k+1))<1.8

        mx=p;

        j=k;

    end

end

figure,imshow(I);
hold on;
rectangle('Position',[BB2(1,j-2),BB2(1,j-1),BB2(1,j),BB2(1,j+1)],'EdgeColor','r' )
% hObject    handle to face (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over original.


% --- Executes on button press in pushbutton9.
function pushbutton9_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in test1.
function test1_Callback(hObject, eventdata, handles)
i=imread('Toys_Candy.jpg');
imtool(i);

% hObject    handle to test1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in test2.
function test2_Callback(hObject, eventdata, handles)
i=imread('Toys_Candy.jpg');
Igray = rgb2gray(i);
Ithres = im2bw(Igray,0.5);
figure,imshowpair(i, Ithres, 'montage');
% hObject    handle to test2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in test3.
function test3_Callback(hObject, eventdata, handles)
I = imread('Toys_Candy.jpg');
Im=I;

rmat=Im(:,:,1);
gmat=Im(:,:,2);
bmat=Im(:,:,3);


levelr = 0.63;
levelg = 0.5;
levelb = 0.4;
i1=im2bw(rmat,levelr);
i2=im2bw(gmat,levelg);
i3=im2bw(bmat,levelb);
Isum = (i1&i2&i3);

% Plot the data
figure,subplot(2,2,1), imshow(i1);
title('Red Plane');
subplot(2,2,2), imshow(i2);
title('Green Plane');
subplot(2,2,3), imshow(i3);
title('Blue Plane');
subplot(2,2,4), imshow(Isum);
title('Sum of all the planes');




% hObject    handle to test3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in test4.
function test4_Callback(hObject, eventdata, handles)


I = imread('Toys_Candy.jpg');
Im=I;

rmat=Im(:,:,1);
gmat=Im(:,:,2);
bmat=Im(:,:,3);


levelr = 0.63;
levelg = 0.5;
levelb = 0.4;
i1=im2bw(rmat,levelr);
i2=im2bw(gmat,levelg);
i3=im2bw(bmat,levelb);
Isum = (i1&i2&i3);


%% Complement Image and Fill in holes
Icomp = imcomplement(Isum);
Ifilled = imfill(Icomp,'holes');


%%
se = strel('disk', 25);
Iopenned = imopen(Ifilled,se);
% figure,imshowpair(Iopenned, I);


%% Extract features

Iregion = regionprops(Iopenned, 'centroid');
[labeled,numObjects] = bwlabel(Iopenned,4);
stats = regionprops(labeled,'Eccentricity','Area','BoundingBox');
areas = [stats.Area];
eccentricities = [stats.Eccentricity];

%% Use feature analysis to count skittles objects
idxOfSkittles = find(eccentricities);
statsDefects = stats(idxOfSkittles);

figure, imshow(I);
hold on;
for idx = 1 : length(idxOfSkittles)
        h = rectangle('Position',statsDefects(idx).BoundingBox,'LineWidth',2);
        set(h,'EdgeColor',[.75 0 0]);
        hold on;
end
if idx > 10
title(['There are ', num2str(numObjects), ' objects in the image!']);
end
hold off;
% hObject    handle to test4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in test5.
function test5_Callback(hObject, eventdata, handles)
%% Read in image
I = imread('Toys_Candy.jpg');
%% RGB Color space
rmat=I(:,:,1);
gmat=I(:,:,2);
bmat=I(:,:,3);

figure;
subplot(2,2,1), imshow(rmat);
title('Red Plane');
subplot(2,2,2), imshow(gmat);
title('Green Plane');
subplot(2,2,3), imshow(bmat);
title('Blue Plane');
subplot(2,2,4), imshow(I);
title('Original Image');


%% HSV Color space
H = rgb2hsv(I);

H_plane=H(:,:,1);
S_plane=H(:,:,2);
V_plane=H(:,:,3);

figure;
subplot(2,2,1), imshow(H_plane);
title('H Plane');
subplot(2,2,2), imshow(S_plane);
title('S Plane');
subplot(2,2,3), imshow(V_plane);
title('V Plane');
subplot(2,2,4), imshow(I);
title('Original Image');

%%  LAB Color Spaces
RGB = I;

cform = makecform('srgb2lab');
Lab = applycform(RGB,cform);

L_plane=Lab(:,:,1);
a_plane=Lab(:,:,2);
b_plane=Lab(:,:,3);

figure;
subplot(2,2,1), imshow(L_plane);
title('L Plane');
subplot(2,2,2), imshow(a_plane);
title('a Plane');
subplot(2,2,3), imshow(b_plane);
title('b Plane');
subplot(2,2,4), imshow(I);
title('Original Image');
% hObject    handle to test5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
